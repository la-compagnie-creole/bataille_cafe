﻿using System;
using System.Collections.Generic;

namespace Cafe
{
    class Program
    {
       
        static void Main(string[] args)
        {
            Game game = new Game("127.0.0.1", 1213);
            Client CLI = new Client("127.0.0.1", 1213);

            game.Start();
            game.EndGame();
            CLI.Disconnect();
        }

   
    }
}
