﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;

namespace Cafe
{
	public class Client
	{
		private string server;
		private int port;
		private string carte;
		private TcpClient TCPclient;
		private NetworkStream stream;
		
		char[,] m_initialPlateau = new char[10,10];
		List<string> m_listCoodParcelleFree = new List<string>();

		//Local map's location
		private string fileLoc;

		// Constructeur d'un Client
        // IN[String]: Addresse IP du serveur et port 
        // OUT[Client]: Retourne un objet Client
		public Client(string server, int port) {
			this.server = server;
			this.port = port;
			this.TCPclient = Connect();
			this.stream = this.TCPclient.GetStream();
		}
		
		public Client(string fileLoc) {
			this.fileLoc = fileLoc;
		}

		// Connection TCP au serveur
        // IN[]: 
        // OUT[TcpClient]: Retourne un objet TcpClient
		public TcpClient Connect() {
			return new TcpClient(this.server, this.port);
		}

		// Fermeture de la connexion TCP  
		public void Disconnect() {
			System.Console.WriteLine("déconnexion ...");
			this.stream.Close();
			this.TCPclient.Close();
		}
		
		// Récupere la carte du serveur
        // IN[]: 
        // OUT[]: 
		public void GetCarteFromServer() {
			byte[] data = new byte[512];
			Int32 bytes = stream.Read(data, 0, data.Length);
    		this.carte = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
		}

 		// Permet de lire le contenu d'un fichier local
        // OUT[]: Chaine de caractere initialisé avec le continu du fichier
		public void GetCarteFromLocalFile() {
			this.carte = System.IO.File.ReadAllText(this.fileLoc);
		}

		// Retourne la carte récupéré par la méthode GetCarteFromServer() 
        // IN[]: 
        // OUT[String]: Retourne une chaine de caractère
		public string GetCarte() {
			return this.carte;
		}

		// Envoie au serveur du coup par notre Client
		// IN[String]:  Une chaine de caractère de format `A:xy`
		// OUT[String]: Une chaine de caractère indiquant si le coup est valable
		public void SendCoup(string coup) {
			Byte[] data = System.Text.Encoding.ASCII.GetBytes(coup);
			// Send the message to the connected TcpServer.
			stream.Write(data, 0, data.Length);
			Console.WriteLine("Sent: {0}", coup);
		}

		// Place la première graine sur la plus grande parcelle 
		public string PremierCoup(char[,] plateau) {
            int[] parcelles = new int[17];
			int coupX = 0, coupY = 0;
            int premiercoup = 1;
            int deb = 97;
            int fin = 113;
            System.Console.WriteLine("comptage de parcelles");
            for(int i = 0; i < 10; i++) {
                for(int j = 0; j < 10; j++) {
                    if(plateau[i,j] >= deb || plateau[i,j] <= fin) {
                        for(int o = 0; o < 17; o++) {
                            if(plateau[i,j] == deb+o) {
                                parcelles[o] += 1;
								if(parcelles[o] > premiercoup) {
									premiercoup = parcelles[o];
									coupX = i;
									coupY = j;
								}
                            }
                        }
                    }
                }
            }
			Console.WriteLine("x = {0}, y = {1} ", coupX, coupY);
			return String.Format("A:{0}{1}", coupX, coupY);
        }
		
		public string SendCoupManuel(string x, string y)
		{
			return String.Format("A:{0}{1}", x, y);
		}

		public string ReceiveCoup() {
			// Mémoire tampon stockant la réponse du serveur
			Byte[] data = new Byte[4];
			String reponseServ = String.Empty;
			Int32 bytes = this.stream.Read(data, 0, data.Length);
			reponseServ = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
			return reponseServ;
		}

		public string GetScoreFinal() {
			Byte[] data = new Byte[7];
			String scoreFinal;
			Int32 bytes = this.stream.Read(data, 0, data.Length);
			scoreFinal = Encoding.ASCII.GetString(data, 0, bytes);
			return scoreFinal;
		}

		// Calcul un coefficient de valeur pour chaque une des parcelles pour déterminer sur laquelle il faudra planter par la suite
		// IN[char[,], string]:  Le plateau de jeu en cours de partie, les coordonnées du coup du Serveur au format "21"
		// OUT[List<double>]: Les coefficients permettant de déterminer quelle parcelle est la plus pertinante
		public List<double> DefineParcelleToPlant (char[,] p_plateau, string p_coordResponseServ)
		{
			List<double> v_ListCoeffParcelle = new List<double>();

			List<char> v_listLetterParcelleFree = GetLetterParcelleFree(p_plateau, p_coordResponseServ);
			foreach (char letter in v_listLetterParcelleFree)
			{
				double v_coeffParcelle;
				int v_nbCaseByParcelle = 0;
				int v_nbCaseServeurByParcelle = 0;
				int v_nbCaseClientByParcelle = 0;
				List<string> v_listCoordParcelle = GetCoordParcelle(letter);
				foreach (string indice in v_listCoordParcelle)
				{
					if (p_plateau[Convert.ToInt16(indice.Substring(0, 1)), Convert.ToInt16(indice.Substring(1, 1))] == '1')
					{
						v_nbCaseClientByParcelle++;
					}
					if (p_plateau[Convert.ToInt16(indice.Substring(0, 1)), Convert.ToInt16(indice.Substring(1, 1))] == '2')
					{
						v_nbCaseServeurByParcelle++;
					}
					v_nbCaseByParcelle++;
				}
				
				if ((v_nbCaseByParcelle / 2) + 1 - v_nbCaseServeurByParcelle > 0)
				{
					if ((v_nbCaseByParcelle / 2) + 1 - v_nbCaseClientByParcelle > 0)
						v_coeffParcelle = v_nbCaseByParcelle;
					else
						v_coeffParcelle = 0;
				}
				else
					v_coeffParcelle = 0;
				
				v_ListCoeffParcelle.Add(v_coeffParcelle);
			}

			return v_ListCoeffParcelle;
		}

		// Permet de déterminer entre plusieurs case d'une même parcelle laquelle choisir en fonction des cases adjacentes
		public string DefineCaseParcelleToPlant(List<double> p_listCoeffParcelle, List<char> p_listParcelleLetterFree, string p_coordResponseServ, char[,] p_plateau)
		{
			double v_maxCoeff = p_listCoeffParcelle.Max(coeff => coeff);
			List<string> v_coordParcelle = GetCoordParcelle(p_listParcelleLetterFree[p_listCoeffParcelle.IndexOf(v_maxCoeff)]);
			Console.WriteLine("lettre: " + p_listParcelleLetterFree[p_listCoeffParcelle.IndexOf(v_maxCoeff)]);
			List<string> v_coordParcelleSameRowColumn = new List<string>();
			List<int> v_nbCaseClientAdjacent = new List<int>();
			foreach(string coordonnee in v_coordParcelle)
			{
				if ((coordonnee.Substring(0,1) == p_coordResponseServ.Substring(2,1) || coordonnee.Substring(1, 1) == p_coordResponseServ.Substring(3, 1)) && p_plateau[Convert.ToInt32(coordonnee.Substring(0,1)), Convert.ToInt32(coordonnee.Substring(1, 1))]!='1' && p_plateau[Convert.ToInt32(coordonnee.Substring(0, 1)), Convert.ToInt32(coordonnee.Substring(1, 1))] != '2')
				{
					int x = Convert.ToInt16(coordonnee.Substring(0, 1));
					int y = Convert.ToInt16(coordonnee.Substring(1, 1));
					int v_nbCaseClient = 0;
					if(x!=0)
						if (p_plateau[x - 1, y] == '1')
							v_nbCaseClient++;
					if(x!=9)
						if (p_plateau[x + 1, y] == '1')
							v_nbCaseClient++; 
					if(y!=0)
						if (p_plateau[x, y - 1] == '1')
							v_nbCaseClient++; 
					if(y!=9)
						if (p_plateau[x, y + 1] == '1')
							v_nbCaseClient++;
					v_nbCaseClientAdjacent.Add(v_nbCaseClient);
					v_coordParcelleSameRowColumn.Add(coordonnee);
				}
			}
			int v_maxNbCase = v_nbCaseClientAdjacent.Max(coeff => coeff);
			
			string v_parcelleChoosed = ("A:" + v_coordParcelleSameRowColumn[v_nbCaseClientAdjacent.IndexOf(v_maxNbCase)]);

			return v_parcelleChoosed;
		}

		// Initialise le plateau vide de début de partie
		public void InitialisePlateau(char[,] p_plateau)
		{
			for(int x = 0; x < 10; x++)
			{
				for(int y = 0; y < 10; y++)
				{
					m_initialPlateau[x, y] = p_plateau[x, y];
				}
			}

		}

		//Permet de retourner la liste de parcelles libres en horizontal et vertical en fonction de la coordonnée jouée par le serveur passé en paramètre
		public List<char> GetLetterParcelleFree(char[,] p_plateau, string p_coordResponseServ) 
		{
			List<char> v_listLetterParcelle = new List<char>();
			int x = Convert.ToInt16(p_coordResponseServ.Substring(2, 1));
			int y = Convert.ToInt16(p_coordResponseServ.Substring(3, 1));
			m_listCoodParcelleFree.Clear();
			v_listLetterParcelle.Add(m_initialPlateau[x, y]);

			for(int X=0; X < 10; X++)
			{
				if(p_plateau[X, y]!='1' && p_plateau[X, y] != '2' && p_plateau[X, y] != 'M' && p_plateau[X, y] != 'F')
				{
					if (!v_listLetterParcelle.Contains(m_initialPlateau[X, y]))
					{
						Console.WriteLine(p_plateau[X, y]);
						Console.WriteLine("x: " + X + " y: "+y);
						v_listLetterParcelle.Add(p_plateau[X, y]);
						m_listCoodParcelleFree.Add(X + "" + y);
					}
				}
			}
			for(int Y=0; Y<10; Y++)
			{
				if (p_plateau[x, Y] != '1' && p_plateau[x, Y] != '2' && p_plateau[x, Y] != 'M' && p_plateau[x, Y] != 'F')
				{
					if (!v_listLetterParcelle.Contains(m_initialPlateau[x, Y]))
					{
						Console.WriteLine(p_plateau[x, Y]);
						Console.WriteLine("x: " + x + " y: "+Y);

						v_listLetterParcelle.Add(p_plateau[x, Y]);
						m_listCoodParcelleFree.Add(x + "" + Y);
					}
				}
			}
			v_listLetterParcelle.RemoveAt(0);
			return v_listLetterParcelle;
		}

		// Permet de récupérer toutes les coordonnées d'une parcelle passée en paramètre
		public List<string> GetCoordParcelle(char p_letterParcelle)
		{
			List<string> v_listCoordParcelle = new List<string>();

			for(int x=0; x < 10; x++) 
			{
				for(int y=0; y <10; y++)
				{
					if (m_initialPlateau[x, y] == p_letterParcelle)
					{
						v_listCoordParcelle.Add(x +""+ y);
					}
				}
			}

			return v_listCoordParcelle;
		}

	}
}
