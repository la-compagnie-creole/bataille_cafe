using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace Cafe
{
    public class Game
    { 
        public Client Client;
        char[,] Plateau;
        
        public Game(string server, int port) {
            this.Client = new Client(server, port);
            this.Plateau = new char[10, 10];
            RecupererCarte();
            Client.InitialisePlateau(Plateau);
        }

        public Client GetClient() {
			return this.Client;
		}

        // Récupere la carte du serveur
        // IN[]: 
        // OUT[]: 
        public void RecupererCarte() {
            string File;
            int[,] IlotEntier = new int[10, 10];
			string[] TramesFile = new string[10];
            GetClient().GetCarteFromServer();
            // Récupération de la trame
            File= GetClient().GetCarte();
            Decrypt decrypt = new Decrypt(File);

			TramesFile = decrypt.cutLines();
            IlotEntier = decrypt.cutSeparator(TramesFile);
            Plateau = decrypt.TransliterationMerForet(IlotEntier);
            Plateau = decrypt.TransliterationLetters(IlotEntier, Plateau);
        }

        // Retourne le plateau(carte) récupéré par la méthode RecupererCarte() 
        // IN[]: 
        // OUT[Char[]]: Retourne un tableau 2D de charactère
        public char[,] GetPlateau() {
            return this.Plateau;
        }

        // Plante une graine sur la carte
        // In[char, int, int] : Joueur = 1 ou 2; Les deux entiers correspondent aux coordonnées
        // OUT:
        public void PlanterGraine(string position) {
            char Joueur;
            if(position[0] == 'A')
                Joueur = '1';
            else
                Joueur = '2';
            int x = Int32.Parse(position[2].ToString()); 
            int y = Int32.Parse(position[3].ToString());
            this.Plateau[x,y] = Joueur;
        }

        // Réalise l'exécution de la partie
        public void Start() {
            string coupServeur;
            string coupClient;

            Client.InitialisePlateau(Plateau);

            Affichage.PrintCarte(Plateau);

            //Envoi du premier coup
            coupClient = GetClient().PremierCoup(GetPlateau());
            /*
            Random random = new Random();
            coup = ("A:" + random.Next(1, 8)+ random.Next(1, 8));
            */
            GetClient().SendCoup(coupClient);
            Console.WriteLine("1er coup: " + coupClient);
       
            System.Console.WriteLine("Coup valide ?");
            if (ReponseCoup(GetClient().ReceiveCoup()))
            {
                System.Console.WriteLine("oui");
                PlanterGraine(coupClient);
                Affichage.PrintCarte(Plateau);
            }
            else
            {
                System.Console.WriteLine("non");
            }

            //Récupération du coup Serveur
            coupServeur = GetClient().ReceiveCoup();
            if (EnvoieCoup(coupServeur)) {
                System.Console.WriteLine("Recupération et plantage ...");
                Console.WriteLine("coup serveur: " + coupServeur);
                Affichage.PrintCarte(Plateau);
            } 
            else {
                System.Console.WriteLine("impossible de récup le coup");
            }

            //Vérification si partie fini ou non
            while(ContinuerPartie(GetClient().ReceiveCoup())) 
            {
                System.Console.WriteLine("La partie continue");
                coupClient = Client.DefineCaseParcelleToPlant(Client.DefineParcelleToPlant(Plateau, coupServeur), Client.GetLetterParcelleFree(Plateau, coupServeur), coupServeur, Plateau);
                System.Console.WriteLine("coup = {0}", coupClient);
                GetClient().SendCoup(coupClient);

                System.Console.WriteLine("Coup valide ?");
                if (ReponseCoup(GetClient().ReceiveCoup()))
                {
                    System.Console.WriteLine("oui");
                    PlanterGraine(coupClient);
                }
                else
                {
                    System.Console.WriteLine("non");
                }
                Affichage.PrintCarte(Plateau);

                coupServeur = GetClient().ReceiveCoup();
                if (EnvoieCoup(coupServeur))
                {
                    System.Console.WriteLine("Recupération et plantage ...");
                    System.Console.WriteLine("coup Serveur = {0}", coupServeur);
                    Affichage.PrintCarte(Plateau);
                }
                else
                {
                    System.Console.WriteLine("impossible de récup le coup");
                }
            }
            
        }

        // Vérifie si le coup du client est valide ou non 
        public bool ReponseCoup(string reponse) {
            //System.Console.WriteLine("reponse : {0}", reponse);
            if(String.Equals(reponse,"VALI"))
                return true;
            else
                return false;
        }

        // Plante la graine du serveur dans le plateau (carte) du client
        public bool EnvoieCoup(string reponse) {
            if(!String.Equals(reponse, "FINI")) {
                PlanterGraine(reponse);
                return true;
            }
            else {
                return false;
            }
        }

        // Vérifie si la partie est finie ou non
        public bool ContinuerPartie(string reponse) {
            if(String.Equals(reponse, "ENCO"))
                return true;
            else{
                System.Console.WriteLine("FIN DU JEU");    
                return false;
            }
        }

        // Affiche le score final et déconnecte le client du serveur
        public void EndGame() {
            System.Console.WriteLine("game is over");
            System.Console.WriteLine("score final : {0}", GetClient().GetScoreFinal());

            GetClient().Disconnect();
        }
    }
}