﻿using System;
using System.IO;
using System.Text; //StringBuilder

namespace Cafe
{
    public class Decrypt
    {
        private string trame;
        public Decrypt(string trame) {
            this.trame = trame;
        }

        // Réalise un passage à la ligne à chaque séparateurs "|"
        // IN[String]: Chaine de caractère à découper
        // OUT[String[]]: Tableau à une dimension contenant tous les passages à la ligne
		public string[] cutLines() {
			return this.trame.Split("|"); 
		}

        // Permet de découper les chaines de caractères contenues dans un tableau grace aux séparateurs ':' entre chaque valeurs
        // IN[String[]]: Tableau à une dimension à découper
        // OUT[int[,]]: Tableau à deux dimensions sans séparateurs
        public int[,] cutSeparator(string[] str) {
            int[,] ilotEntier = new int[10,10];
            int result;
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    string ch = str[x].Split(':')[y];
                    Int32.TryParse(ch, out result);
                    ilotEntier[x,y] = result;
                }
            }
            return ilotEntier;
        }

        // Réalise la transliteration des parcelles Mer et Foret à partir d'un tableau d'entiers à deux dimensions suivant les règles du jeu "La bataille du café"
        // IN[int[,]] Tableau à convertir 
        // OUT[char[,]] Tableau de caractères résultant de la conversion
        public char[,] TransliterationMerForet(int[,] ilotInteger)
        {  
            char[,] plateau = new char[10, 10];
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    if (ilotInteger[x,y] >= 64)
                    {
                        plateau[x, y] = 'M';
                    }
                    else if (ilotInteger[x,y] >= 32)
                    {
                        plateau[x, y] = 'F';
                    }
                }
            }
            return plateau;
        }
        
        // Réalise la transliteration des parcelles lettres à partir d'un tableau d'entiers à deux dimensions suivant les règles du jeu "La bataille du café"
        // IN[int[,], char[,]] Tableau à convertir et plateau final créé avec la méthode TransliterationMerForet
        // OUT[char[,]] Tableau de caractères résultant de la conversion
        public char[,] TransliterationLetters(int[,] ilotInteger, char[,] plateau)
        {
            char[] tabParcelles = new char[20] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's','t'};
            int indParcelle = -1;
            
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    if (ilotInteger[x,y] < 32)
                    { 
                        if (ilotInteger[x,y] >= 8)
                        {
                            ilotInteger[x,y] = ilotInteger[x,y] - 8;
                        }
                        if (ilotInteger[x,y] >= 4)
                        {
                            ilotInteger[x,y] = ilotInteger[x,y] - 4;
                        }
                        if (ilotInteger[x,y] == 2)
                        {
                            plateau[x, y] = plateau[x - 1, y];
                        }
                        if (ilotInteger[x,y] <= 1)
                        {
                            plateau[x, y] = plateau[x, (y - 1)];
                        }
                        if (ilotInteger[x,y] == 3)
                        {
                            if(ilotInteger[x,(y+1)] == 4 || ilotInteger[x,(y+1)] == 12 || ilotInteger[x,(y+1)] == 0)
                            {
                            plateau[x, y] = plateau[x-1, y+1];
                            }
                            else{
                            indParcelle = indParcelle + 1;
                            plateau[x, y] = tabParcelles[indParcelle];
                            }
                        }
                    }
                }
            }
            return plateau;
        }
    }
}
